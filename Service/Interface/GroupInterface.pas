unit GroupInterface;

interface

uses
  superobject;

type
  IGroupInterface = interface
    function add(data: ISuperObject): Boolean;
    function edit(data: ISuperObject): Boolean;
    function deletebyid(id: Integer): Boolean;
    function getList(): ISuperObject;
  end;

implementation

end.

