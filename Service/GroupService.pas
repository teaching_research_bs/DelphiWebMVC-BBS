unit GroupService;

interface

uses
  GroupInterface, superobject, uConfig, uTableMap, BaseService;

type
  TGroupService = class(TBaseService, IGroupInterface)
  public
    function add(data: ISuperObject): Boolean;
    function edit(data: ISuperObject): Boolean;
    function deletebyid(id: Integer): Boolean;
    function getList: ISuperObject;
  end;

implementation

{ TGroupService }

function TGroupService.add(data: ISuperObject): Boolean;
begin

end;

function TGroupService.deletebyid(id: Integer): Boolean;
begin

end;

function TGroupService.edit(data: ISuperObject): Boolean;
begin

end;

function TGroupService.getList: ISuperObject;
begin
  Result := Db.Find(tb_group, '')
end;

end.

