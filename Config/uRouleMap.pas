unit uRouleMap;

interface

uses
  Roule;

type
  TRouleMap = class(TRoule)
  public
    constructor Create(); override;
  end;

implementation

uses
  IndexController, BBSController, LoginController;

constructor TRouleMap.Create;
begin
  inherited;
  //·��,������,��ͼĿ¼
  SetRoule('', TIndexController, 'index');
  SetRoule('login', TLoginController, 'login');
  SetRoule('bbs', TBBSController, 'bbs');
end;

end.

