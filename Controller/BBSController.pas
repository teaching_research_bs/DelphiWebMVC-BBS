unit BBSController;

interface

uses
  BaseController, System.SysUtils;

type
  TBBSController = class(TBaseController)
    procedure index();
    procedure getContent;
    procedure content;
  end;

implementation

uses
  GroupInterface, GroupService, ContentInterface, ContentService, superobject,
  DiscussInterface, DiscussService;

{ TMainController }
var
  group_service: IGroupInterface;
  content_service: IContentInterface;
  discuss_service: IDiscussInterface;

procedure TBBSController.content;
var
  id: Integer;
begin
  group_service := TGroupService.Create(view.Db);
  content_service := TContentService.Create(View.Db);
  discuss_service := TDiscussService.Create(View.Db);
  with view do
  begin

    id := InputInt('id');
    setAttr('data', content_service.getById(id).AsString);
    setAttr('discuss', discuss_service.getList(id).AsString);
    setAttr('group', group_service.getList.AsString);
    ShowHTML('content');
  end;
end;

procedure TBBSController.getContent;
var
  json: ISuperObject;
  data: ISuperObject;
  con: integer;
  group_id: string;
  page, limit: integer;
begin
  content_service := TContentService.Create(View.Db);
  with view do
  begin

    page := InputInt('page');
    limit := InputInt('limit');
    group_id := Input('groupid');
    if group_id = '' then
      group_id := '-1';

    json := content_service.getPage(con, page - 1, limit, StrToInt(group_id));
    ShowPage(con, json);
  end;
end;

procedure TBBSController.index;
begin
  group_service := TGroupService.Create(View.Db);
  with View do
  begin
    setAttr('user', SessionGet('user'));
    setAttr('groupid', Input('groupid'));
    setAttr('group', group_service.getList.AsString);
    ShowHTML('index');
  end;
end;

end.

